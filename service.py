from keras.models import load_model
import numpy as np


def compute(data):
    model = load_model("./service/model")

    data_reshapped = np.array(data).reshape(1, -1)
    prediction = model.predict(data_reshapped)

    prediction_corrected = float(prediction[0][0]) - 0.05

    prediction_formatted = prediction_corrected
    result = {
        "class": prediction_formatted, 
        }

    return result
